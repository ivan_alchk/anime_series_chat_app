package com.alchuk.anime_series_chat_serverside.controller;

import com.alchuk.anime_series_chat_serverside.exceptions.UserAlreadyExistsException;
import com.alchuk.anime_series_chat_serverside.exceptions.UserNotFoundException;
import com.alchuk.anime_series_chat_serverside.model.User;
import com.alchuk.anime_series_chat_serverside.service.UserService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/users")
public class UserController {

    private final UserService userService;


    @Autowired
    public UserController(UserService userService){
        this.userService = userService;
    }



   @ExceptionHandler({UserAlreadyExistsException.class, MethodArgumentNotValidException.class, UserNotFoundException.class})
   public ResponseEntity<String> handleException(Exception e){

        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);

   }





    @PostMapping("/signUp")
    @ResponseBody
    public User signUp(@Validated @RequestBody User user){


        if(userService.getUserByUserName(user.getUserName()) != null){

            throw new UserAlreadyExistsException("Such user already exists, please sign in");

        }

        userService.addUser(user);

        return user;
    }


    @GetMapping("/signIn")
    @ResponseBody
    public User signIn(@RequestParam String userName, @RequestParam String password, HttpSession session){
         User user = userService.getUserByUserNameAndPassword(userName, password);
        if(user != null){
            session.setAttribute("userId", user.getUserID());
            return user;
        }
        throw new UserNotFoundException("Incorrect username or password");

    }

    @PutMapping("/updateUserName/{newUserName}")
    @ResponseBody
    public ResponseEntity<String> updateUserName(@PathVariable String newUserName, HttpSession session){
        long userId = (Long)session.getAttribute("userId");
        userService.updateUserName(newUserName, userId);

        return new ResponseEntity<>("Username update successfully", HttpStatus.OK);

    }





}
