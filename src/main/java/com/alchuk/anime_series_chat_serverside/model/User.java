package com.alchuk.anime_series_chat_serverside.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.validation.constraints.*;

@Component
@Scope("prototype")
public class User {


    @Getter
    @Setter
    private long userID;

    @Getter
    @Setter
    @NotBlank
    private String userName;

    @Getter
    @Setter
    @NotBlank
    private String password;

    @Getter
    @Setter
    @NotBlank
    @Email
    private String email;




}
