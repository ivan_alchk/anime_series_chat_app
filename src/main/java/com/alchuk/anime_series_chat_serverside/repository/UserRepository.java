package com.alchuk.anime_series_chat_serverside.repository;

import com.alchuk.anime_series_chat_serverside.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface UserRepository {

     void addUser(String userName, String password, String email);

     User getUserByID(long ID);

     List<User> getAllUsers();

     User getUserByName(String userName);

     void updateUserName(String userName, long id);

     void updatePassword(String password, long id);

     void deleteUser(long id);


    User getUserByUserNameAndPassword(String userName, String password);
}
