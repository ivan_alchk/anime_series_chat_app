package com.alchuk.anime_series_chat_serverside.repository;

import com.alchuk.anime_series_chat_serverside.model.User;
import com.alchuk.anime_series_chat_serverside.util.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;


import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {


    private final JdbcTemplate jdbcTemplate;
    @Autowired
    private UserMapper userMapper;


    @Autowired
    public UserRepositoryImpl(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }


    @Override
    public void addUser(String userName, String password, String email) {
        String sql = String.format("INSERT INTO USERS (USER_NAME, PASSWORD, EMAIL) VALUES('%s','%s','%s')", userName, password, email);
        jdbcTemplate.execute(sql);


    }

    @Override
    public User getUserByID(long ID) {
        String sql = String.format("SELECT * FROM USERS WHERE USER_ID = '%d'", ID);
        return jdbcTemplate.queryForObject(sql, userMapper);
    }

    @Override
    public User getUserByName(String userName) {
        try {
            String sql = String.format("SELECT * FROM USERS WHERE USER_NAME = '%s'", userName);

            return jdbcTemplate.queryForObject(sql, userMapper);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }

    }

    @Override
    public User getUserByUserNameAndPassword(String userName, String password) {

        try {
            String sql = String.format("SELECT * FROM USERS WHERE USER_NAME = '%s' AND PASSWORD = '%s'", userName, password);
            return jdbcTemplate.queryForObject(sql, userMapper);
        } catch (EmptyResultDataAccessException e) {
            return null;

        }


    }

    @Override
    public List<User> getAllUsers() {
        String sql = "SELECT * FROM USERS";
        return jdbcTemplate.query(sql, userMapper);
    }

    @Override
    public void updateUserName(String userName, long id) {
        String sql = String.format("UPDATE USERS SET USER_NAME = '%s' WHERE USER_ID = '%d'", userName, id);
        jdbcTemplate.execute(sql);

    }

    @Override
    public void updatePassword(String password, long id) {
        String sql = String.format("UPDATE USERS SET PASSWORD = '%s' WHERE USER_ID = '%d'", password, id);
        jdbcTemplate.execute(sql);

    }

    @Override
    public void deleteUser(long id) {
        String sql = String.format("DELETE FROM USERS WHERE USER_ID = '%d'", id);
        jdbcTemplate.execute(sql);

    }
}
