package com.alchuk.anime_series_chat_serverside.exceptions;

public class UserAlreadyExistsException extends RuntimeException {


    private final String message;

    public UserAlreadyExistsException(String message){
        this.message = message;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
