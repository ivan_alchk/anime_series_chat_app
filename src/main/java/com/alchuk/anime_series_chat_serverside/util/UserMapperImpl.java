package com.alchuk.anime_series_chat_serverside.util;


import com.alchuk.anime_series_chat_serverside.AppConfig;
import com.alchuk.anime_series_chat_serverside.model.User;

import lombok.SneakyThrows;

import org.springframework.context.ApplicationContext;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;


import java.sql.ResultSet;

@Component
public class UserMapperImpl implements UserMapper {




    @Override



    @SneakyThrows
    public User mapRow(ResultSet resultSet, int i) {


        User user = new User();
        user.setUserID(resultSet.getLong("USER_ID"));
        user.setUserName(resultSet.getString("USER_NAME"));
        user.setPassword(resultSet.getString("PASSWORD"));
        user.setEmail(resultSet.getString("EMAIL"));

        return user;
    }
}


