package com.alchuk.anime_series_chat_serverside.util;

import com.alchuk.anime_series_chat_serverside.model.User;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

@Component
public interface UserMapper extends RowMapper<User> {
}
