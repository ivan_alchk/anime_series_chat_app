package com.alchuk.anime_series_chat_serverside.service;

import com.alchuk.anime_series_chat_serverside.model.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {


    void addUser(User user);

    User getUserByID(long id);

    User getUserByUserName(String userName);

    List<User>getAllUsers();

    void updateUserName(String userName, long id);

    void updatePassword(String password, long id);

    void deleteUser(long id);


    User getUserByUserNameAndPassword(String userName, String password);
}
