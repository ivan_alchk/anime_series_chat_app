package com.alchuk.anime_series_chat_serverside.service;

import com.alchuk.anime_series_chat_serverside.model.User;
import com.alchuk.anime_series_chat_serverside.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserServiceImpl implements UserService {


    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository){
        this.userRepository = userRepository;

    }

    @Override
    public void addUser(User user) {
        String userName = user.getUserName();
        String password = user.getPassword();
        String email = user.getEmail();
        userRepository.addUser(userName, password, email);

    }

    @Override
    public User getUserByUserName(String userName) {
        return userRepository.getUserByName(userName);
    }


    @Override
    public User getUserByID(long id) {
        return userRepository.getUserByID(id);
    }

    @Override
    public User getUserByUserNameAndPassword(String userName, String password) {
        return userRepository.getUserByUserNameAndPassword(userName, password);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.getAllUsers();
    }

    @Override
    public void updateUserName(String userName, long id) {
        userRepository.updateUserName(userName, id);

    }

    @Override
    public void updatePassword(String password, long id) {
        userRepository.updatePassword(password, id);

    }

    @Override
    public void deleteUser(long id) {
        userRepository.deleteUser(id);

    }
}
