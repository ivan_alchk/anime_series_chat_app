package com.alchuk.anime_series_chat_serverside;

import com.alchuk.anime_series_chat_serverside.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import javax.sql.DataSource;

@SpringBootApplication
public class AnimeSeriesChatServersideApplication {



    public static void main(String[] args) {
       ConfigurableApplicationContext configurableApplicationContext = SpringApplication.run(AnimeSeriesChatServersideApplication.class, args);

       for(String name : configurableApplicationContext.getBeanDefinitionNames()){
           System.out.println(name);
       }



    }

}
